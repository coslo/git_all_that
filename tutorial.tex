% Created 2022-04-22 Fri 20:23
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
%\usepackage[OT1]{fontenc}
\linespread{1.1}
\textwidth=16cm
\textheight=22cm
\hoffset=-1.8cm
\voffset=-2.2cm
\parindent=0ex
\parskip=1ex
\usepackage[dvipsnames]{xcolor}
%\usepackage{courier}
\usepackage{microtype}
\usepackage{listings}
%\usepackage{cmbright}
%\renewcommand{\sfdefault}{cmss}
%\renewcommand{\familydefault}{\sfdefault}
\lstset{basicstyle=\ttfamily,columns=fullflexible,breaklines=true,showstringspaces=false,language=bash}
\lstset{commentstyle=\color{Gray},keywordstyle=\bfseries,stringstyle=\color{BrickRed}}
\lstset{frame=l,framexleftmargin=5pt,framerule=1pt,rulecolor=\color{Gray}}
\usepackage{enumitem}
\setlist{itemsep=0pt,parsep=0pt,topsep=2pt,leftmargin=*}
\renewcommand{\labelitemi}{--}
\makeatletter\def\verbatim@font{\color{Gray}\ttfamily}\makeatother  % grey font in verbatim
\usepackage{titling}
\usepackage{float}
\setlength{\droptitle}{-40pt}
\pretitle{\begin{center}\bfseries\large}
\predate{\begin{center}\vskip-10pt}
\hypersetup{
colorlinks,%
citecolor=black,%
filecolor=black,%
linkcolor=blue,%
urlcolor=blue
}
\usepackage{etoolbox}
\makeatletter
\preto{\@verbatim}{\topsep=-40pt \partopsep=40pt }
\makeatother
\usepackage{tocloft}
\setlength{\cftbeforesecskip}{6pt}
\author{Daniele Coslovich}
\date{\today}
\title{Git, github, gitlab and all that}
\hypersetup{
 pdfauthor={Daniele Coslovich},
 pdftitle={Git, github, gitlab and all that},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.1.13)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents

\newpage

\section{Outline}
\label{sec:orgb78a880}

This is a short tutorial on:
\begin{enumerate}
\item \textbf{git} \ldots{}
\item \ldots{} \textbf{github}, \textbf{gitlab} and all that
\end{enumerate}

The materials and source are available online: \url{https://framagit.org/coslo/git\_all\_that}

Tutorial as a pdf file: \url{https://framagit.org/coslo/git\_all\_that/-/blob/master/tutorial.pdf}

Authoritative sources of information on git:
\begin{itemize}
\item The \emph{official} git documentation: \url{https://git-scm.com/doc}
\item Beginner tutorial by \emph{gitlab}: \url{https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html}
\item Beginner tutorial by \emph{github}: \url{https://docs.github.com/en/github/getting-started-with-github/quickstart}
\item Questions on \emph{stack overflow}: \url{https://stackoverflow.com/questions/tagged/git}
\end{itemize}

\section{Git in a nutshell}
\label{sec:orgf7914e0}

\begin{center}
\includegraphics[width=0.2\linewidth]{./images/Git-logo.png}
\end{center}


Git is a \textbf{version control system} (VCS)

\begin{itemize}
\item Keep \textbf{history} of modifications to files
\item Single-user or team \textbf{software development}
\item Created by Linus Torvalds in 2005 (\(\rightarrow\) linux kernel)
\item As of 2022, the \textbf{most popular} VCS out there
\end{itemize}

It is a \emph{command line tool}! Check if you have it:
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git --version
\end{lstlisting}

\begin{verbatim}
git version 2.25.1
\end{verbatim}

\textbf{Found it?} Tell it here \url{https://app.wooclap.com/ainfo}. If not: \url{https://git-scm.com/downloads}

\section{What do \emph{I} use it for?}
\label{sec:org7a5d697}

Over the past 10+ years:
\begin{itemize}
\item \textbf{code development}
\item \textbf{configuration files} (my \texttt{.bashrc}, \texttt{.emacs}, etc.)
\item \textbf{research papers} - with \emph{geek} colleagues only :-(
\item website, curriculum, tutorials, notebooks, \ldots{}
\end{itemize}

All this can be done
\begin{itemize}
\item as a \textbf{single user} or in \textbf{collaboration} with others
\item both \textbf{offline} on a machine (ex. my laptop) or\ldots{}
\item \ldots{} over a \textbf{network} (I currently use \url{https://framagit.org/} as main server)
\end{itemize}

\section{Quick start}
\label{sec:orgd6b9461}
To keep track of the modification history of the files in a folder, turn that folder into a git \textbf{repository}.

Increments in the modification history appear as \texttt{commits}: each commit stores a set of modifications to the project, like \textbf{adding}, \textbf{modifying} or \textbf{removing} files.

Create a new folder and initialize the repository
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
mkdir project
cd project
git init
\end{lstlisting}

\begin{verbatim}
Initialized empty Git repository in /home/coslo/usr/git_gitlab_allthat/project/.git/
\end{verbatim}

Add a README file in the \texttt{project} folder
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
echo "Hello World!" > README.md
git status
\end{lstlisting}

\begin{verbatim}
On branch master

No commits yet

Untracked files:
..." to include in what will be committed)

	README.md

nothing added to commit but untracked files present (use "git add" to track)
\end{verbatim}

Now add the file to the project and commit the change
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git add .  # the dot will add all new files in the folder
git commit -m "Initial commit"
\end{lstlisting}

\begin{verbatim}
[master (root-commit) 6598ad9] Initial commit
 1 file changed, 1 insertion(+)
 create mode 100644 README.md
\end{verbatim}

\section{More on commits}
\label{sec:org9b2a445}

As you can see, this is done in two steps:
\begin{itemize}
\item \texttt{add}: include the new file into the list of modifications to record
\item \texttt{commit}: actually store the modifications in the repository
\end{itemize}

The \texttt{-m} flag allows you to leave a "commit message" to describe the change.

Modify the file and check the difference
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
echo "Hello World, again!" >> README.md
git diff
\end{lstlisting}

\begin{verbatim}
diff --git a/README.md b/README.md
index 980a0d5..5eb476f 100644
--- a/README.md
+++ b/README.md
@@ -1 +1,2 @@
 Hello World!
+Hello World, again!
\end{verbatim}

Commit the change
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git commit -am "Update README"
\end{lstlisting}

\begin{verbatim}
[master e7d25eb] Update README
 1 file changed, 1 insertion(+)
\end{verbatim}

If we modify some files, we can "add" the change and commit in one shot with the \texttt{-a} flag.

Have a look at what we have down so far
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git log
\end{lstlisting}

\begin{verbatim}
commit e7d25ebebc747cfbd50cdf0de842f1c008202d20 (HEAD -> master)
Author: Daniele Coslovich <92140-coslo@users.noreply.framagit.org>
Date:   Thu Apr 15 22:42:52 2021 +0200

    Update README

commit 6598ad9651c45b8b026bfe20d69010183cc17adb
Author: Daniele Coslovich <92140-coslo@users.noreply.framagit.org>
Date:   Thu Apr 15 22:42:07 2021 +0200

    Initial commit
\end{verbatim}

\emph{Moral}: useful commands to check the status of your repository are
\begin{itemize}
\item \texttt{git status}
\item \texttt{git diff}
\item \texttt{git log}
\end{itemize}

Finally, removing a file is similar to adding it
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
touch useless_file.txt
git add useless_file.txt
git commit -m "Add useless file"

git rm useless_file.txt
git commit -m "Remove useless file"
\end{lstlisting}

\begin{verbatim}
[master 8bd1bf4] Add useless file
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 useless_file.txt
rm 'useless_file.txt'
[master a639553] Remove useless file
 1 file changed, 0 insertions(+), 0 deletions(-)
 delete mode 100644 useless_file.txt
\end{verbatim}

\section{Fine-tuning}
\label{sec:org4166ec9}

\textbf{Adding your username and email to commits}

This is not necessary, but you may want to specify your name and e-mail (actually, git will prompt you to do that on the first commit)
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git config user.name <your_name>
git config user.email <your_email>
\end{lstlisting}
Add the \texttt{-{}-global} flag to apply these settings globally for all your repositories

\emph{Privacy notes}:

\begin{itemize}
\item If you plan to share your project publicly, consider using a dummy (inexistent) email account
\item git commits may reveal a lot of information about a user's habits\ldots{}
\end{itemize}

\textbf{Existing files}

What if the folder \emph{already} contains files and/or sub-folders and we want to some (but not all) of them to the repository?

\emph{Option 1}: add all the files then remove the ones you do not need
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git add .
git rm <not_needed_file>
git commit -m "Add files"
\end{lstlisting}

\emph{Option 2}: add selected files
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git add <needed_file_1> <needed_file_2>
git commit -m "Add files"
\end{lstlisting}

You can use the star syntax (ex. \texttt{*.tex}) to match multiple file

\textbf{Ignoring files}

You may want to avoid storing certain files in your git repository, for instance
\begin{itemize}
\item temporary or backup files (\texttt{*.\textasciitilde{}}, \texttt{*.bak})
\item compilation artifacts and executables (\texttt{*.o}, \texttt{*.so}, \texttt{*.x})
\end{itemize}

Create a text file called \texttt{.gitignore} at the root of your project and fill it with patterns matching the files to ignore
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
cat > .gitignore <<EOF
*~
*.o
EOF

git add .gitignore
git commit -am "Add gitignore"
\end{lstlisting}

From now on, \texttt{git status} will ignore these files and they will not be considered for commit

\section{FAQ}
\label{sec:org90993fa}

\textbf{How do I go back in time?}

First look for the \emph{hash} of the commit you are interested in

\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git log --pretty=oneline
\end{lstlisting}

\begin{verbatim}
a63955319bd09c01fcd09e529f7444b9a25beae5 (HEAD -> master) Remove useless file
8bd1bf4391833c204dd977ae2ae2b37b02aad31f Add useless file
e7d25ebebc747cfbd50cdf0de842f1c008202d20 Update README
6598ad9651c45b8b026bfe20d69010183cc17adb Initial commit
\end{verbatim}

Check out a copy of the project as it was in that commit
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git checkout e7d25eb
\end{lstlisting}

To get back to the current state of the project
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git checkout master
\end{lstlisting}

\textbf{Do I \emph{really} need to write those commit messages?}

Writing commit messages may seem a burden (and actually, you can leave them blank) but it \emph{is} important.

\emph{Tip}: you may be able to write commit messages directly from your IDE and/or text editor.

\url{https://chris.beams.io/content/images/size/w2000/2021/01/git\_commit\_2x.png}

There are well established guidelines to write \textbf{good} commit messages, so that they are informative and easy to read
\url{https://chris.beams.io/posts/git-commit/}

\textbf{Structure}
\begin{itemize}
\item \emph{Separate subject from body with a blank line}
\item Use the body to explain what and why vs. how
\end{itemize}

\textbf{Style}
\begin{itemize}
\item \emph{Use the imperative mood in the subject line}
\item Capitalize the subject line
\item Do not end the subject line with a period
\item Limit the subject line to 50 characters
\item Wrap the body at 72 characters
\end{itemize}

Example: write " Fix typo in tutorial", not " fixed typo in tutorial."

\section{Software versioning}
\label{sec:orgf9fd72c}

For basic usage, \emph{explicit} versioning is not necessary: use git hashes to identify commits. However, it is \emph{crucial} for libraries and API, or any code on which \emph{other} codes may depend upon.

\begin{itemize}
\item \texttt{X}: simple incremental versioning
\item \texttt{X.Y}: major-minor (ex: \texttt{0.1}, that's where you start from)
\item \texttt{X.Y.Z}: major-minor-patch (ex. git \texttt{2.17.1})
\item \texttt{X.Y.Z.*}: alpha / beta / release candidate info
\end{itemize}

There is only one sensible strategy to decide how to increment versions: \textbf{semantic versioning}
\url{https://semver.org}

In a nutshell:
\begin{itemize}
\item increase \texttt{X}:  backward incompatible changes
\item increase \texttt{Y}:  new features
\item increase \texttt{Z}:  bug fixes
\end{itemize}

Explicit versioning in git is handled via \texttt{tags}. 

Tags are snapshots of the state of a project to which git assigns a meaningful identifier

\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git commit -m "Bump version 0.1.0" 
git tag -am "0.1.0" 0.1.0
\end{lstlisting}

List all tags
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git tag -l
\end{lstlisting}

\begin{verbatim}
0.1.0
\end{verbatim}

Check out the project at a specific tag
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git checkout 0.1.0
\end{lstlisting}

Use \texttt{git checkout master} to get back to the current state of the project.

\section{Branches}
\label{sec:orge74dd3a}

Branches allow you to keep \textbf{multiple versions of your project} in parallel.

Repositories are created with a default branch, typically called \textbf{master} or \textbf{main}. That's the branch to which we committed the modifications above.

Typical use case: develop a new feature in a separate \emph{feature branch}, then merge it in the master branch.
\begin{center}
\includegraphics[width=.9\linewidth]{images/branch1.png}
\end{center}

Glossary:
\begin{itemize}
\item \emph{Fast-forward}: no commits between the creation of the new branch and the merge
\item \emph{Conflict}: concurrent modification to the same part of a file
\end{itemize}

Create a new branch and check it out
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git checkout -b new_cool_feature
# commit commit commit ...
\end{lstlisting}

\begin{verbatim}
Switched to a new branch 'new_cool_feature'
\end{verbatim}

Show available branches
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git branch -va
\end{lstlisting}

\begin{verbatim}
master           a639553 Remove useless file
* new_cool_feature a639553 Remove useless file
\end{verbatim}

The prompt of your terminal should tell you which branch you are working on
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
coslo@local:~/usr/git_gitlab_allthat/ainfo(master*)$
\end{lstlisting}

If not, you may want to follow these instructions  \url{https://blog.sellorm.com/2020/01/13/add-the-current-git-branch-to-your-bash-prompt/}

When the new feature is ready, get back to the master branch and merge the feature branch
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git checkout master
git merge new_cool_feature
\end{lstlisting}

If \textbf{conflicts} occur, you will have to modify the files to remove them. \texttt{git status} will tell you how to proceed.

\emph{Beware}: conflict handling can be nasty\ldots{}

\section{Task 1: single user workflow}
\label{sec:orgb07f3d8}

\begin{itemize}
\item Make sure \texttt{git} is installed on your machine (if not, follow \url{https://git-scm.com/downloads})
\item Download and extract the python template project \texttt{ainfo}
\end{itemize}
\url{https://framagit.org/coslo/git\_all\_that/-/blob/master/ainfo.zip}
\begin{itemize}
\item Turn it into a new git repository: add all the relevant files and commit your changes
\item Edit \texttt{README.md} by adding your name to the "Authors" section and commit the change
\end{itemize}
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
Authors
-------
...
\end{lstlisting}
\begin{itemize}
\item Familiarize yourself to the commands seen above (except those related to branching)
\end{itemize}

\section{Task 2: feature branches and testing}
\label{sec:org326e30f}

\begin{itemize}
\item Create a new branch add the following function to the \texttt{helpers} module
\end{itemize}
\lstset{language=Python,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
def fibonacci(n):
    """Return the n_th Fibonacci number"""
    if n < 0:
        raise ValueError("n must be >= 0")
    elif n == 0:
        return 0
    elif n == 1 or n == 2:
        return 1
    else:
        return fibonacci(n-1) + fibonacci(n-2)
\end{lstlisting}
\begin{itemize}
\item Add a test in \texttt{tests/test\_helpers.f90} (follow the instructions!)
\item Execute \texttt{make test} from the command line
\item If the test is passed, commit all the changes
\item Get back to the master \texttt{branch} and merge the feature branch
\end{itemize}

\emph{Note}: in this case, the merge actually boils down to a \texttt{fast forward}

\section{Distributed git}
\label{sec:orgc64d558}

Git is a \textbf{distributed VCS}: several copies of the same repository may exist and exchange with one another.

Multiple instances of a project may exist:
\begin{itemize}
\item \textbf{locally} on a machine
\item over a \textbf{network}, communicating through various protocols (ssh, git, https)
\end{itemize}

From the perspective of a given repository, other repositories of the same project are called \emph{remotes}.

\emph{Note}: once remotes are set up, the workflow is identical whether the repository copies are on the same hard disk or on a different machine.

Typically one keeps a repository as an "official" source for the project. This official repository must be a \texttt{bare} one: there are no checked out files in it.

Example:
\begin{itemize}
\item two repositories cloned from \texttt{project}
\item \texttt{project\_1} is a remote of \texttt{project\_2}, named \texttt{extra}
\item all repositories are "local", i.e. on the same disk
\end{itemize}
\begin{center}
\includegraphics[width=.9\linewidth]{images/dia0.png}
\end{center}

Initialize a bare "official" repository in the \texttt{project} folder and clone two copies
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git init --bare project
git clone project project_1
git clone project project_2
\end{lstlisting}

\begin{center}
\includegraphics[width=.9\linewidth]{images/dia0b.png}
\end{center}

Add \texttt{project\_1} as a remote of \texttt{project\_2}
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
cd project_2
git remote add extra ../project_1
\end{lstlisting}

The syntax is  \texttt{git remote add <remote\_name> <remote\_path>}
\begin{itemize}
\item \texttt{<remote\_name>} is a name for the remote
\item \texttt{<remote\_path>} is the path to the remote (can be over the network, see below)
\end{itemize}

Have a look at the remote repositories we have access to
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git remote show
\end{lstlisting}
By default, the \texttt{origin} remote is the one we cloned the project from.

We can now check out any branch of the \texttt{project\_1} remote repository, inspect or grab files from them, merge them into our own branches.

Example: the master branch of \texttt{project\_1} contains some interesting commit we want to merge in our code
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git pull extra master
git checkout extra/master
# ... look at code here
git checkout master
git merge extra/master
\end{lstlisting}

That's an actual use case of mine: use a repository for code development and clone local copies in each new research project

\begin{center}
\includegraphics[width=.9\linewidth]{images/dia1.png}
\end{center}

Push new commits from the development project to the bare one
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
cd development
# commit commit commit ...
git push
\end{lstlisting}

Go to \texttt{project\_1} and pull the new changes
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
cd ../project_1
git pull
\end{lstlisting}

To collaborate remotely, users must have access to a git server. A git server may boil down to a simple ssh server, to which you and your collaborators have access to
\begin{center}
\includegraphics[width=.9\linewidth]{images/dia2.png}
\end{center}

Of course, one may keep multiple remote repositories on different servers
\begin{center}
\includegraphics[width=.9\linewidth]{images/dia3.png}
\end{center}

\section{Github, gitlab and all that}
\label{sec:org30bbdc3}

git is a \textbf{decentralized} VCS: no central server, every repository can share with any other.

However, git servers have undergone a coarsening process and a few big \textbf{centralized} servers ("silos") have emerged.

Along this process, \textbf{web applications} grew around git servers, adding functionalities that allow users to browse repositories and collaborate on software:

\begin{itemize}
\item \textbf{github}: centralized, closed source, owned by Microsoft (\url{https://github.com/explore})
\item \textbf{gitlab}: centralized, open-source edition (\url{https://gitlab.com/explore/projects/trending})
\item self-hosted instances of gitlab: ex. \textbf{framagit} (\url{https://framagit.org/})
\item \textbf{gittea} (\url{https://gitea.io/en-us/}), \textbf{gogs} (\url{https://gogs.io/}): self-hosted
\item ActivityPub for federation
\item \ldots{}
\end{itemize}

Features:

\begin{itemize}
\item issue tracking: report bugs or ask for new features
\item pull requests: facilitate merging of new features and bug fixes
\item continuous integration: automatic testing and deployment of code on multiple platforms
\item web page hosting: host static web pages (ex. documentation)
\item social networking: collaborate and discover new projects
\end{itemize}

Anti-features:

\begin{itemize}
\item social networking (!)
\item walled gardens
\item over-engineered
\end{itemize}

\section{Creating an account}
\label{sec:orgdd29fa4}

Both gitlab and github have free plans for \emph{private} and \emph{public} repositories. Here I create an account on gitlab \url{https://gitlab.com/users/sign\_up} (no need to give your actual identity)

\begin{center}
\includegraphics[width=.9\linewidth]{./images/gitlab1.png}
\end{center}

\begin{center}
\includegraphics[width=.9\linewidth]{./images/gitlab2.png}
\end{center}

\section{Setting up ssh keys}
\label{sec:org830dcca}

Create a pair of private-public ssh keys. This will encrypt your communications to the remote server using the \texttt{git} protocol
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
ssh-keygen
# Press Enter three times... (or add a passphrase for security)
\end{lstlisting}

Your public key is
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
cat ~/.ssh/id_rsa.pub
\end{lstlisting}

\begin{verbatim}
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDnnpooSARIJ6HYdy/QWrtv0C1XANNKWUIDei3BxCXy9f62hzAQMtJuVAr/
wu0WfNnygcP1w+D/LOScdrRrzw5hCHainVcVQustw8OtkyW19ITboMD5KC9P2pdSPtsdaUNF8ZK0+fDfYwFpRfeF0+qsebQs
OWaLF03x+o69ZnJf1Gs6PGER2aETBVz9UUniveMf0CqHGmUK5l5Yj9Otzs4uAAYEyvGPt5amtRWMGufQIB5edrOqa/+Mu8gI
xu6uHUtLsEs9AzDslM/McWDVoDAumnBgJsLGJpD3/0DsD4dToH6NtRsNLhy8NmRotsJwL+KSIrT9zpVfk67 coslo@local
\end{verbatim}

Your private key is
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
cat ~/.ssh/id_rsa
\end{lstlisting}
Never ever share your private key!

In the \emph{SSH Keys} settings on the web interface (top right corner, then \emph{Preferences}), add a new key by pasting the content of \texttt{\textasciitilde{}/.ssh/id\_rsa.pub}

\begin{center}
\includegraphics[width=.9\linewidth]{./images/addssh.png}
\end{center}

\section{Creating a new repository}
\label{sec:orga48d746}

From the web interface, create a new blank project in your namespace

\begin{center}
\includegraphics[width=.9\linewidth]{./images/gitlabproject.png}
\end{center}

Now push your own \texttt{ainfo} git project to the gitlab server.

The default remote is called \texttt{origin}. Add the gitlab server as the new \texttt{origin} remote, then push the \texttt{master} branch over there
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git remote add origin git@gitlab.com:coslo/test.git
git push -u origin master
\end{lstlisting}

To push all existing branches and tags (if you have them)
\lstset{language=sh,label= ,caption= ,captionpos=b,numbers=none}
\begin{lstlisting}
git push -u origin --all
git push -u origin --tags
\end{lstlisting}

\section{Projects with git}
\label{sec:org97749a6}

\begin{itemize}
\item contribute to an open-source project (ex. \url{https://framagit.org/atooms})
\item revision control of a computational project (ex. Peressi's lab project)
\item revision control of a scientific document (ex. tesina triennale)
\item continuous integration (ex. github actions)
\item git for latex documents vs. self-hosted overleaf instance
\item \ldots{}
\end{itemize}

\section{Task 1 (optional): create an account or gitlab or github}
\label{sec:orgccbd37b}

\begin{center}
\includegraphics[width=.9\linewidth]{images/ex2.png}
\end{center}

\begin{itemize}
\item choose a web platform of your choice
\item create an account on it
\item create your ssh keys and add \emph{the public one} in your account
\item create a new private project
\item push your \texttt{ainfo} repository over there
\item browse your repository online
\item \emph{commit, commit, commit\ldots{}} then push the changes to the server again
\end{itemize}

\section{Task 2: gym with multiple repositories}
\label{sec:orgef86363}

\begin{center}
\includegraphics[width=.9\linewidth]{images/ex1.png}
\end{center}

\begin{itemize}
\item clone your repository \texttt{ainfo} as a bare repository and call it \texttt{ainfo\_main}
\item add this bare repository as the origin of \texttt{ainfo}
\item edit a file of you choice in \texttt{ainfo} and commit the change
\item push the changes to \texttt{ainfo\_main}
\end{itemize}

\textbf{Bonus}:
\begin{itemize}
\item clone the \texttt{ainfo\_main} project once more as \texttt{ainfo\_copy}
\item go to \texttt{ainfo\_copy} and add \texttt{ainfo} as a remote called \texttt{extra}
\item you can now pull the changes directly from \texttt{ainfo}
\end{itemize}
\end{document}