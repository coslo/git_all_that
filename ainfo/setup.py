#!/usr/bin/env python

import os

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup
    
with open('README.md') as f:
    readme = f.read()

setup(name='ainfo',
      version='0.1.0',
      description='',
      long_description=readme,
      long_description_content_type="text/markdown",
      author='',
      author_email='',
      url='',
      packages=['ainfo'],
      license='GPLv3',
      install_requires=[],
      scripts=[],
      classifiers=[
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
          'Programming Language :: Python',
      ]
     )
